<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\CompanyService;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class CompanyController extends Controller
{

    /**
     * @var Authenticatable|User|null
     */
    protected Authenticatable|null|User $loggedInUser;
    /**
     * @var CompanyService
     */
    protected CompanyService $companyService;

    public function __construct(CompanyService $companyService)
    {
        $this->loggedInUser = auth()->user();
        $this->companyService = $companyService;
    }

    public function list()
    {

        $companies = $this->loggedInUser->companies()->paginate();

        return response()->json($companies);
    }

    public function create(Request $request): JsonResponse
    {
        $this->validate($request, [
            'phone' => 'required|numeric',
            'title' => 'required|string',
            'description' => 'required|string'
        ]);

        $company = $this->companyService->create($this->loggedInUser, $request->all());

        return response()->json($company);
    }
}
