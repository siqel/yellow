<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$router->post('/user/register', ['uses' => 'AuthController@register']);
$router->get('/user/sign-in', ['uses' => 'AuthController@login']);
$router->post('/user/forget', ['uses' => 'AuthController@recoverPassword']);
$router->post('/user/recover-password', [ 'as' => 'password.reset', 'uses' => 'AuthController@resetPassword' ]);

$router->group(['middleware' => 'auth'], function () use ($router) {
    $router->get('/user/companies', ['uses' => 'CompanyController@list']);
    $router->post('/user/companies', ['uses' => 'CompanyController@create']);
});
