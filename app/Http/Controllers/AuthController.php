<?php

namespace App\Http\Controllers;


use App\Services\UserService;
use Exception;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    use CanResetPassword;

    public function __construct(private UserService $userService)
    {

    }

    /**
     * @throws ValidationException
     */
    public function register(Request $request): JsonResponse
    {
        $this->validate($request, [
            'email' => 'required|unique:users|email',
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required|numeric',
            'password' => 'required|min:6'
        ]);

        return $this->userService->register($request->all());
    }


    public function login(Request $request): JsonResponse
    {

         $this->validate($request, [
             'email' => 'required|email',
             'password' => 'required|min:6'
         ]);
        return $this->userService->login($request->all());
    }

    public function resetPassword(Request $request): JsonResponse
    {
        $this->validate($request, [
            'email' => 'required|email|exists:users',
            'password' => 'required',
            'token' => 'required|string'
        ]);

        return $this->userService->reset($request->all());
    }

    public function recoverPassword(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|exists:users,email',
        ]);

        return $this->userService->recover($request->all());
    }

}
