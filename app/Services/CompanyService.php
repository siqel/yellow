<?php

namespace App\Services;

use App\Models\Company;
use App\Models\User;

class CompanyService
{
    public function create(User $user, array $data): Company
    {
        $company = new Company();
        $company->title = $data['title'];
        $company->description = $data['description'];
        $company->phone = $data['phone'];
        $company->user_id = $user->getKey();
        $company->save();

        return $company;
    }

}
