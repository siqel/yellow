<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Validation\ValidationException;

class UserService
{
    public function __construct(
        private User $model,
    )
    {
    }


    public function register(array $data): JsonResponse
    {
        $user = $this->model::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        return $this->responseWithToken($user);
    }


    public function login($credentials): JsonResponse
    {
        if (!Auth::attempt($credentials)) {
            return response()->json([
                "message" => __('auth.failed'),
            ], 401);
        }

        $token = Auth::guard('api')->attempt($credentials);

        return $this->responseWithToken($token);
    }

    protected function responseWithToken($token): JsonResponse
    {
        return response()->json(
            [
                'token' => $token,
                'token_type' => 'bearer',
                'expires_in' => Auth::factory()->getTTL() * 60
            ],
            200
        );
    }

    public function reset(array $data): JsonResponse
    {
        $emailPasswordStatus = Password::reset(
            [
                'email' => $data['email'],
                'password' => $data['password'],
                'token' => $data['token']
            ], function ($user, $password) {
            $user->password = Hash::make($password);
            $user->save();
        });

        if (Password::INVALID_TOKEN === $emailPasswordStatus) {
            return response()->json([
                "message" => __('auth.failed'),
            ], 401);
        }

        return response()->json(['message' => "Password successfully changed"]);
    }

    public function recover(array $data): JsonResponse
    {
        Password::sendResetLink(['email' => $data['email']]);

        return response()->json(['message' => "Email has successfully been sent, please check your inbox."]);
    }
}
