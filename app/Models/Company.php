<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{

    protected $fillable = [
        'phone', 'title', 'user_id', 'description'
    ];
}
